<?php
/**
 * @package ovic-addon-toolkit/templates
 * @var $shortcode
 * @var $template
 * @var $element
 */
$is_legacy_mode_active = true;

if ($shortcode->elementor_v3) {
    $is_legacy_mode_active = Elementor\Plugin::instance()->get_legacy_mode('elementWrappers');
}

?>
<#
if ( settings.background_video_link ) {
    let videoAttributes = 'autoplay muted playsinline';

    if ( ! settings.background_play_once ) {
        videoAttributes += ' loop';
    }

    view.addRenderAttribute( 'background-video-container', 'class', 'elementor-background-video-container' );

    if ( ! settings.background_play_on_mobile ) {
        view.addRenderAttribute( 'background-video-container', 'class', 'elementor-hidden-phone' );
    }
#>
    <div {{{ view.getRenderAttributeString( 'background-video-container' ) }}}>
        <div class="elementor-background-video-embed"></div>
        <video class="elementor-background-video-hosted elementor-html5-video" {{ videoAttributes }}></video>
    </div>
<# } #>
<div class="elementor-background-overlay"></div>
<div class="elementor-shape elementor-shape-top"></div>
<div class="elementor-shape elementor-shape-bottom"></div>
<div class="elementor-container elementor-column-gap-{{ settings.gap }}">
    <?php if ($is_legacy_mode_active) { ?>
        <#
        view.addRenderAttribute( 'owl-slick', 'class', 'elementor-row' );

        if ( settings._use_slide == 'yes' ) {
            var slide_data = {
                slidesToShow:parseInt(settings.slides_to_show),
                slidesMargin:parseInt(settings.slides_margin),
                rows:parseInt(settings.slides_rows),
                arrows:false,
                dots:false,
            };
            if ( settings.slides_vertical == 'yes' ) {
                slide_data.vertical = true;
            }
            if ( settings.slides_navigation == 'both' || settings.slides_navigation == 'arrows' ) {
                slide_data.arrows = true;
            }
            if ( settings.slides_navigation == 'both' || settings.slides_navigation == 'dots' ) {
                slide_data.dots = true;
            }
            slide_data = JSON.stringify(slide_data);

            view.addRenderAttribute( 'owl-slick', 'class', 'owl-slick' );
            view.addRenderAttribute( 'owl-slick', 'class', settings.slides_rows_space );
            view.addRenderAttribute( 'owl-slick', 'data-slick', slide_data );
        }
        #>
        <div {{{ view.getRenderAttributeString( 'owl-slick' ) }}}></div>
    <?php } ?>
</div>