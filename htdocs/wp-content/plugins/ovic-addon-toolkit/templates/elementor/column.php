<?php
/**
 * @package ovic-addon-toolkit/templates
 * @var $shortcode
 * @var $template
 * @var $element
 */
$is_legacy_mode_active = true;

if ($shortcode->elementor_v3) {
    $is_legacy_mode_active = Elementor\Plugin::instance()->get_legacy_mode('elementWrappers');
    $wrapper_element       = $is_legacy_mode_active ? 'column' : 'widget';
}

?>
<div class="elementor-column-wrap">
    <div class="elementor-background-overlay"></div>
    <?php if ($is_legacy_mode_active) { ?>
        <#
        view.addRenderAttribute( 'owl-slick', 'class', 'elementor-widget-wrap' );

        if ( settings._use_slide == 'yes' ) {
            var slide_data = {
                slidesToShow:parseInt(settings.slides_to_show),
                slidesMargin:parseInt(settings.slides_margin),
                rows:parseInt(settings.slides_rows),
                arrows:false,
                dots:false,
            };
            if ( settings.slides_vertical == 'yes' ) {
                slide_data.vertical = true;
            }
            if ( settings.slides_navigation == 'both' || settings.slides_navigation == 'arrows' ) {
                slide_data.arrows = true;
            }
            if ( settings.slides_navigation == 'both' || settings.slides_navigation == 'dots' ) {
                slide_data.dots = true;
            }
            slide_data = JSON.stringify(slide_data);

            view.addRenderAttribute( 'owl-slick', 'class', 'owl-slick' );
            view.addRenderAttribute( 'owl-slick', 'class', settings.slides_rows_space );
            view.addRenderAttribute( 'owl-slick', 'data-slick', slide_data );
        }
        #>
        <div {{{ view.getRenderAttributeString( 'owl-slick' ) }}}></div>
    <?php } ?>
</div>