(function ($) {
    'use strict';

    var term_debounce = function (callback, threshold, immediate) {
        var timeout;
        return function () {
            var context = this,
                args    = arguments;
            var later   = function () {
                timeout = null;
                if (!immediate) {
                    callback.apply(context, args);
                }
            };
            var callNow = (immediate && !timeout);
            clearTimeout(timeout);
            timeout = setTimeout(later, threshold);
            if (callNow) {
                callback.apply(context, args);
            }
        };
    };

    $(window).on('load', function () {

        // Get default editor settings
        var uid           = window.ovic_term.id,
            settings      = window.ovic_term.settings,
            media_buttons = $(window.ovic_term.buttons),
            shortcode     = media_buttons.find('.ovic-shortcode-button'),
            textarea      = $('#' + uid);

        // Add on change event handle
        var editor_on_change = function (editor) {
            editor.on('change', term_debounce(function () {
                editor.save();
                textarea.trigger('change');
            }, 250));
        };

        // Add editor id to shortcode
        if (shortcode.length) {
            shortcode.attr('data-editor-id', uid);
        }

        // Extend editor selector and on change event handler
        settings.tinymce.setup = editor_on_change;

        // Wait until :visible
        var interval = setInterval(function () {
            if (textarea.is(':visible')) {
                window.wp.editor.initialize(uid, settings);
                clearInterval(interval);
            }
        });

        // Add Media buttons
        setTimeout(function () {
            var editor    = textarea.closest('.wp-editor-wrap'),
                tools     = textarea.closest('.wp-editor-tools'),
                shortcode = media_buttons.find('.ovic-shortcode-button');
            editor.prepend(media_buttons);
        }, 300);

    });

})(jQuery);