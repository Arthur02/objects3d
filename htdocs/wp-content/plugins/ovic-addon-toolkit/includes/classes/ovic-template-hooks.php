<?php
/**
 * Ovic Template Hooks
 *
 * Action/filter hooks used for Ovic functions/templates.
 *
 * @package Ovic/Templates
 * @version 1.0.0
 */

defined('ABSPATH') || exit;

/**
 *
 * REMOVE RAW CONTENT
 **/
add_filter('get_pagenum_link',
    function ($result) {
        $result = remove_query_arg('ovic_raw_content', $result);

        return $result;
    }
);
/**
 *
 * WOOCOMMERCE VARIABLE PRODUCT
 */
add_filter('woocommerce_available_variation', 'ovic_custom_available_variation', 10, 3);
/**
 *
 * RESIZE IMAGE
 **/
add_filter('wp_lazy_loading_enabled', 'ovic_image_lazy_loading', 10, 3);
add_filter('wp_get_attachment_image_attributes', 'ovic_lazy_attachment_image', 10, 3);
/**
 *
 * BUTTON BUY NOW
 **/
add_filter('woocommerce_add_to_cart_redirect', 'ovic_redirect_cart_buy_now', 10, 2);