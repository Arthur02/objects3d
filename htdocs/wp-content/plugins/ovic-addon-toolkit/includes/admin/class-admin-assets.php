<?php
/**
 * Load assets
 *
 * @package     Ovic/Admin
 * @version     2.1.0
 */
if (!defined('ABSPATH')) {
    exit;
}
if (!class_exists('Ovic_Admin_Assets')) :
    /**
     * Ovic_Admin_Assets Class.
     */
    class Ovic_Admin_Assets
    {
        /**
         * Contains an string min file of script handles by Ovic.
         *
         * @var array
         */
        private static $suffix = '';

        /**
         * Hook in tabs.
         */
        public function __construct()
        {
            /* check for developer mode */
            self::$suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

            add_action('admin_enqueue_scripts', array($this, 'admin_styles'));
            add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));

            /**
             * Allow HTML in term (category, tag) descriptions
             */
            if (OVIC_CORE()->get_config('editor_term')) {

                if (!current_user_can('unfiltered_html')) {
                    add_filter('pre_term_description', 'wp_filter_post_kses');
                }

                remove_filter('pre_term_description', 'wp_filter_kses');
                remove_filter('term_description', 'wp_kses_data');

            }
        }

        /**
         * Return asset URL.
         *
         * @param  string  $path  Assets path.
         *
         * @return string
         */
        private static function get_asset_url($path)
        {
            return apply_filters('ovic_get_asset_url', plugins_url($path, OVIC_PLUGIN_FILE), $path);
        }

        /**
         * Enqueue styles.
         */
        public function admin_styles()
        {
            $screen    = get_current_screen();
            $screen_id = $screen ? $screen->id : '';
            // Register admin styles.
            wp_enqueue_style('ovic-admin', self::get_asset_url('assets/admin/ovic-admin.css'), array(), OVIC_VERSION);

            do_action('ovic_admin_style_assets');
        }

        /**
         * Enqueue scripts.
         */
        public function admin_scripts()
        {
            $screen    = get_current_screen();
            $screen_id = $screen ? $screen->id : '';
            // Enqueue term scripts.
            if (OVIC_CORE()->get_config('editor_term')) {
                $this->enqueue_term();
            }
            // Enqueue scripts.
            wp_enqueue_script('ovic-admin', self::get_asset_url('assets/admin/ovic-admin.js'), array('jquery'), OVIC_VERSION, true);

            do_action('ovic_admin_script_assets');
        }

        public function enqueue_term()
        {
            $term_id = '';
            $is_term = false;
            $screen  = get_current_screen();

            if (!empty($screen->base) && $screen->base == 'term') {
                $is_term = true;
                $term_id = 'description';
            }
            if (!empty($screen->base) && $screen->base == 'edit-tags') {
                $is_term = true;
                $term_id = 'tag-description';
            }
            if ($is_term) {
                ob_start();
                echo '<div class="wp-media-buttons" style="position:relative;z-index:2;">';
                do_action('media_buttons');
                echo '</div>';
                $media_buttons = ob_get_clean();
                $settings      = apply_filters('ovic_term_description_wp_editor', array(
                    'tinymce'       => array(
                        'wp_skip_init' => true,
                    ),
                    'quicktags'     => true,
                    'media_buttons' => true,
                ));

                wp_enqueue_editor();

                wp_enqueue_script('ovic-term', self::get_asset_url('assets/admin/ovic-term.js'), array('jquery'), OVIC_VERSION, true);

                wp_localize_script('ovic-term', 'ovic_term', array(
                    'id'       => $term_id,
                    'settings' => $settings,
                    'buttons'  => $media_buttons,
                ));
            }
        }
    }
endif;

return new Ovic_Admin_Assets();
