<?php
/**
 * Ovic Admin
 *
 * @class    Ovic_Admin
 * @author   KuteThemes
 * @category Admin
 * @package  Ovic/Admin
 * @version  1.0.1
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Ovic_Admin class.
 */
if (!class_exists('Ovic_Admin')) {
    class Ovic_Admin
    {
        /**
         * Constructor.
         */
        public function __construct()
        {
            add_action('init', array($this, 'includes'));
            add_action('admin_init', array($this, 'plugin_activate'));
            add_action('current_screen', array($this, 'conditional_includes'));
        }

        /**
         * Dependent plugins
         */
        public function plugin_activate()
        {
            $dependents = array(
                'boutique-toolkit/boutique-toolkit.php',
                'ovic-toolkit/ovic-toolkit.php',
                'mosa-toolkit/toolkit.php',
                'voka-toolkit/toolkit.php',
            );

            foreach ($dependents as $dependent) {
                if (is_plugin_active($dependent)) {
                    deactivate_plugins($dependent);
                }
            }
        }

        /**
         * Include any classes we need within admin.
         */
        public function includes()
        {
            require_once dirname(__FILE__).'/dashboard/dashboard.php';
            include_once dirname(__FILE__).'/class-admin-assets.php';
            include_once dirname(__FILE__).'/admin-functions.php';
        }

        /**
         * Include admin files conditionally.
         */
        public function conditional_includes()
        {
            if (!$screen = get_current_screen()) {
                return;
            }
            switch ($screen->id) {
                case 'options-permalink':
                    include_once dirname(__FILE__).'/class-admin-permalink.php';
                    break;
            }
        }
    }
}

return new Ovic_Admin();
