<?php
/**
 * Name:  Carparts 03
 **/
?>
<header id="header" class="header style-05_3">
    <div class="container">
        <div class="header-wrap megamenu-wrap">
            <div class="header-logo">
                <?php armania_get_logo(); ?>
            </div>
            <div class="header-content">
                <?php armania_header_message(); ?>
                <div class="header-mid">
                    <div class="header-inner">
                        <?php armania_header_search(); ?>
                        <div class="header-control">
                            <div class="inner-control">
                                <?php
                                if ( function_exists( 'armania_header_wishlist' ) ) armania_header_wishlist();
                                if ( function_exists( 'armania_header_mini_cart' ) ) armania_header_mini_cart();
                                armania_header_menu_bar();
                                armania_header_user();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bot">
                    <div class="header-inner">
                        <div class="box-header-nav">
                            <?php armania_header_primary_menu(); ?>
                        </div>
                        <div class="header-right">
                            <?php armania_header_submenu( 'header_topmenu' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
