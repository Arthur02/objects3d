<?php

class Editor_Ovic_Products
{
    public static function shortcode_config()
    {
        return array(
            'name'   => 'ovic_products',
            'title'  => 'Products',
            'fields' => array(
                'title'         => array(
                    'id'    => 'title',
                    'type'  => 'text',
                    'title' => esc_html__('Title', 'armania'),
                ),
                'list_style'    => array(
                    'id'      => 'list_style',
                    'type'    => 'text',
                    'class'   => 'ovic-hidden',
                    'default' => 'none',
                    'title'   => esc_html__('List style', 'armania'),
                ),
                'product_style' => array(
                    'id'          => 'product_style',
                    'type'        => 'select_preview',
                    'title'       => esc_html__('Product style', 'armania'),
                    'options'     => armania_product_options('Shortcode'),
                    'default'     => 'style-01',
                    'description' => esc_html__('Select a style for product item', 'armania'),
                ),
                'target'        => array(
                    'id'          => 'target',
                    'type'        => 'select',
                    'title'       => esc_html__('Target', 'armania'),
                    'options'     => array(
                        'recent_products'       => esc_html__('Recent Products', 'armania'),
                        'featured_products'     => esc_html__('Feature Products', 'armania'),
                        'sale_products'         => esc_html__('Sale Products', 'armania'),
                        'best_selling_products' => esc_html__('Best Selling Products', 'armania'),
                        'top_rated_products'    => esc_html__('Top Rated Products', 'armania'),
                        'products'              => esc_html__('Products', 'armania'),
                        'product_category'      => esc_html__('Products Category', 'armania'),
                        'related_products'      => esc_html__('Products Related', 'armania'),
                    ),
                    'attributes'  => array(
                        'data-depend-id' => 'target',
                    ),
                    'default'     => 'recent_products',
                    'description' => esc_html__('Choose the target to filter products', 'armania'),
                ),
                'ids'           => array(
                    'id'          => 'ids',
                    'type'        => 'select',
                    'chosen'      => true,
                    'multiple'    => true,
                    'sortable'    => true,
                    'ajax'        => true,
                    'options'     => 'posts',
                    'query_args'  => array(
                        'post_type' => 'product',
                    ),
                    'title'       => esc_html__('Products', 'armania'),
                    'description' => esc_html__('Enter List of Products', 'armania'),
                    'dependency'  => array('target', '==', 'products'),
                ),
                'category'      => array(
                    'id'          => 'category',
                    'type'        => 'select',
                    'chosen'      => true,
                    'ajax'        => true,
                    'options'     => 'categories',
                    'placeholder' => esc_html__('Select Products Category', 'armania'),
                    'query_args'  => array(
                        'hide_empty' => true,
                        'taxonomy'   => 'product_cat',
                    ),
                    'title'       => esc_html__('Product Categories', 'armania'),
                    'description' => esc_html__('Note: If you want to narrow output, select category(s) above. Only selected categories will be displayed.',
                        'armania'),
                    'dependency'  => array('target', '!=', 'products'),
                ),
                'limit'         => array(
                    'id'          => 'limit',
                    'type'        => 'number',
                    'unit'        => 'items(s)',
                    'default'     => '6',
                    'title'       => esc_html__('Limit', 'armania'),
                    'description' => esc_html__('How much items per page to show', 'armania'),
                ),
                'orderby'       => array(
                    'id'          => 'orderby',
                    'type'        => 'select',
                    'title'       => esc_html__('Order by', 'armania'),
                    'options'     => array(
                        ''              => esc_html__('None', 'armania'),
                        'date'          => esc_html__('Date', 'armania'),
                        'ID'            => esc_html__('ID', 'armania'),
                        'author'        => esc_html__('Author', 'armania'),
                        'title'         => esc_html__('Title', 'armania'),
                        'modified'      => esc_html__('Modified', 'armania'),
                        'rand'          => esc_html__('Random', 'armania'),
                        'comment_count' => esc_html__('Comment count', 'armania'),
                        'menu_order'    => esc_html__('Menu order', 'armania'),
                        'price'         => esc_html__('Price: low to high', 'armania'),
                        'price-desc'    => esc_html__('Price: high to low', 'armania'),
                        'rating'        => esc_html__('Average Rating', 'armania'),
                        'popularity'    => esc_html__('Popularity', 'armania'),
                        'post__in'      => esc_html__('Post In', 'armania'),
                        'most-viewed'   => esc_html__('Most Viewed', 'armania'),
                    ),
                    'description' => sprintf(esc_html__('Select how to sort retrieved products. More at %s.',
                        'armania'),
                        '<a href="'.esc_url('http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters').'" target="_blank">'.esc_html__('WordPress codex page',
                            'armania').'</a>'),
                ),
                'order'         => array(
                    'id'          => 'order',
                    'type'        => 'select',
                    'title'       => esc_html__('Sort order', 'armania'),
                    'options'     => array(
                        ''     => esc_html__('None', 'armania'),
                        'DESC' => esc_html__('Descending', 'armania'),
                        'ASC'  => esc_html__('Ascending', 'armania'),
                    ),
                    'description' => sprintf(esc_html__('Designates the ascending or descending order. More at %s.',
                        'armania'),
                        '<a href="'.esc_url('http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters').'" target="_blank">'.esc_html__('WordPress codex page',
                            'armania').'</a>'),
                ),
            ),
        );
    }
}